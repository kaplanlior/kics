# KICS analyzer changelog

## v0.1.0
- Propagate loglevel to kics (!8)

## v0.0.4
- Move sarif package to the report dependency (!5)

## v0.0.3
- Lowercase scanner id to be kics (!6)

## v0.0.2
- Switch to KICS (!1)

## v0.0.1
- init terrscan analyzer
