ARG SCANNER_VERSION=v1.4.7

FROM checkmarx/kics:${SCANNER_VERSION}-alpine as kics
FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .

# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM alpine:latest

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

COPY --from=kics --chown=root:root /app/bin/assets /usr/local/bin/assets
COPY --from=kics --chown=root:root /app/bin/kics /usr/local/bin/kics
COPY --from=build --chown=root:root /go/src/app/analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
